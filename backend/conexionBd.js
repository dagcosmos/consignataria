const mongoose = require('mongoose');

mongoose.connect('mongodb://127.0.0.1/consignataria', { useNewUrlParser: true })
.then( db => console.log('Base de Datos (consignataria) Concetada.') )
.catch( err => console.error(err) );

module.exports = mongoose;