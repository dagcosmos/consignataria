const express = require('express');
const controladorArrendatario = require('../controladores/arrendatario');

const enrutador = express.Router();

enrutador.get('/', controladorArrendatario.traerTodos);
enrutador.get('/:id', controladorArrendatario.traer);
enrutador.get('/contratos/:id', controladorArrendatario.traerContratos);
enrutador.post('/', controladorArrendatario.crear);
enrutador.put('/:id', controladorArrendatario.editar);
enrutador.delete('/:id', controladorArrendatario.eliminar);

module.exports = enrutador;