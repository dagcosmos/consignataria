const express = require('express');
const controladorContrato = require('../controladores/contrato');

const enrutador = express.Router();

enrutador.get('/', controladorContrato.traerTodos);
enrutador.get('/:id', controladorContrato.traer);
enrutador.get('/detalle/:id', controladorContrato.traerDetalle);
enrutador.post('/', controladorContrato.crear);
enrutador.put('/:id', controladorContrato.editar);
enrutador.delete('/:id', controladorContrato.eliminar);

module.exports = enrutador;