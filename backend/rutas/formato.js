const express = require('express');
const controladorFormato = require('../controladores/formato');

const enrutador = express.Router();

enrutador.get('/', controladorFormato.traerTodos);
enrutador.get('/:id', controladorFormato.traer);
enrutador.post('/', controladorFormato.crear);
enrutador.put('/:id', controladorFormato.editar);
enrutador.delete('/:id', controladorFormato.eliminar);

module.exports = enrutador;