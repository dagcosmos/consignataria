const express = require('express');
const controladorVehiculo = require('../controladores/vehiculo');

const enrutador = express.Router();

enrutador.get('/', controladorVehiculo.traerTodos);
enrutador.get('/:id', controladorVehiculo.traer);
enrutador.get('/contratos/:id', controladorVehiculo.traerContratos);
enrutador.post('/', controladorVehiculo.crear);
enrutador.put('/:id', controladorVehiculo.editar);
enrutador.delete('/:id', controladorVehiculo.eliminar);

module.exports = enrutador;