const mongoose = require('mongoose');
const { Schema } = mongoose;

const esquemaArrendatario = new Schema({
    nombres: {type: String, required: true},
    apellidos: {type: String, required: true},
    documento: {type: Number, required: true, index: true, unique: true},
    direccion: {type: String, required: true},
    telefono: {type: Number, required: true}
});

module.exports = mongoose.model('arrendatario', esquemaArrendatario);