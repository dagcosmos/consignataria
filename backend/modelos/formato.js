const mongoose = require('mongoose');
const { Schema } = mongoose;

const esquemaFormato = new Schema({
    arrendador: {type: String, required: true},
    ciudad: {type: String, required: true},
    documento: {type: Number, required: true},
    direccion: {type: String, required: true},
    telefono: {type: Number, required: true},
    encabezado: {type: String, required: false},
    nombre: {type: String, required: true},
    clausulas: {type: String, required: false},
});

module.exports = mongoose.model('formato', esquemaFormato);