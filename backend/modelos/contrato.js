const mongoose = require('mongoose');
const { Schema } = mongoose; 
let fecha = new Date();

const esquemaContrato = new Schema({
    fecha: {type: Date, required: true, default: Date.now},
    hora: {type: String, required: true, default: fecha.getHours() + ':' + fecha.getMinutes() + ':' + fecha.getSeconds()},
    valor: {type: Number, required: true},
    vigencia: {type: Date, required: true},
    antelacion_prorroga: {type: Number, required: true, default: 1},
    _id_vehiculo: {type: Schema.ObjectId, ref: 'vehiculo'},
    _id_arrendatario: {type: Schema.ObjectId, ref: 'arrendatario'}
});

module.exports = mongoose.model('contrato', esquemaContrato);