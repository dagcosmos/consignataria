const mongoose = require('mongoose');
const { Schema } = mongoose;

const esquemaVehiculo = new Schema({
    vehiculo: {type: String, required: true, default: "MOTOCICLETA"},
    marca: {type: String, required: true},
    modelo: {type: Number, required: true},
    tipo: {type: String, required: true, default: "TURISMO"},
    color: {type: String, required: true},
    manifiesto: {type: String, required: true},
    placa: {type: String, required: true, index: true, unique: true},
    chasis: {type: String, required: true, index: true, unique: true},
    servicio: {type: String, required: true, default: "PARTICULAR"},
    precio: {type: Number, required: true},
    seguro: {type: String, required: false, default: "SI"},
    vence: {type: String, required: false, default: "SI"},
    numero_seguro: {type: String, required: false, default: "SI"},
    tecnomecanica: {type: String, required: false, default: "SI"},
    propietario: {type: String, required: true},
    documento: {type: Number, required: true},
});

module.exports = mongoose.model('vehiculo', esquemaVehiculo);