const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const { mongoose } = require('./conexionBd');

const servidor = express();

//configuraciones
servidor.set('puerto', process.env.PORT || 3000);

//middelware
servidor.use(morgan('dev'));
servidor.use(express.json());
servidor.use( cors({origin: 'http://127.0.0.1:4200'}) );

//rutas
servidor.use('/arrendatario', require('./rutas/arrendatario'));
servidor.use('/vehiculo', require('./rutas/vehiculo'));
servidor.use('/contrato', require('./rutas/contrato'));
servidor.use('/formato', require('./rutas/formato'));
servidor.listen(servidor.get('puerto'), () => {
    console.log('Servidor escuchando en puerto ', servidor.get('puerto'));
});


