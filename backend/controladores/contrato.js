const controladorContrato = {};
const modeloContrato = require('../modelos/contrato');
const mongoose = require('mongoose');

/**Devuelve todos los documentos de la coleccion Contrato*/
controladorContrato.traerTodos = async (req, res) => 
{
    const registros = await modeloContrato.find()
    .populate({ path: '_id_vehiculo' })
    .populate({ path: '_id_arrendatario' })
    .exec();
    res.json(registros);    
}

/**Devuelve un solo documento con el _id de la coleccion Contrato*/
controladorContrato.traer = async (req, res) => 
{
    const registro = await modeloContrato.findById(req.params.id)
    .populate({ path: '_id_vehiculo' })
    .populate({ path: '_id_arrendatario' })
    .exec();;
    res.json(registro);
}

/**Devuelve el documento de la coleccion Contrato con la asociacion de los documentos de Arrendatario y Vehiculo*/
controladorContrato.traerDetalle = async (req, res) => 
{
    const registros = await modeloContrato.find({_id: req.params.id})
    .populate({ path: '_id_arrendatario'})
    .populate({ path: '_id_vehiculo' })
    .exec();
    res.json(registros);    
}

/**Crea un documento en la coleccion Contrato*/
controladorContrato.crear = async (req, res) => 
{
    const modelo = new modeloContrato({        
        fecha: req.body.fecha,
        hora: req.body.hora,
        valor: req.body.valor,
        vigencia: req.body.vigencia,
        antelacion_prorroga: req.body.antelacion_prorroga,
        _id_vehiculo: req.body._id_vehiculo,
        _id_arrendatario: req.body._id_arrendatario
    });
    await modelo.save();
    res.json({"estado": "Contrato Guardado."});
}

/**Edita un documento segun el _id en la coleccion Contrato */
controladorContrato.editar = async (req, res) => 
{
    const modelo = new modeloContrato({
        _id: req.params.id,
        fecha: req.body.fecha,
        hora: req.body.hora,
        valor: req.body.valor,
        vigencia: req.body.vigencia,
        antelacion_prorroga: req.body.antelacion_prorroga,
        _id_vehiculo: req.body._id_vehiculo,
        _id_arrendatario: req.body._id_arrendatario
    });
  
    await modeloContrato.findByIdAndUpdate(req.params.id, {$set: modelo}, {new: true});
    res.json({"estado": "Contrato Actualizado."});
}

/**Elimina un documento segun el _id en la coleccion Contrato */
controladorContrato.eliminar = async (req, res) => 
{
    await modeloContrato.findByIdAndDelete(req.params.id);
    res.json({"estado": "Contrato Eliminado."});
}

module.exports = controladorContrato;