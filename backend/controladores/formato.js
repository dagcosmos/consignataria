const controladorFormato = {};
const modeloFormato = require('../modelos/formato');

/**Devuelve todos los documentos de la coleccion Formato*/
controladorFormato.traerTodos = async (req, res) => 
{
    const registros = await modeloFormato.find();
    res.json(registros);
}

/**Devuelve un solo documento con el _id de la coleccion Formato*/
controladorFormato.traer = async (req, res) => 
{
    const registro = await modeloFormato.findById(req.params.id);
    res.json(registro);
}

/**Crea un documento en la coleccion Formato*/
controladorFormato.crear = async (req, res) => 
{
    const modelo = new modeloFormato({        
        arrendador: req.body.arrendador,
        ciudad: req.body.ciudad,
        documento: req.body.documento,
        direccion: req.body.direccion,
        telefono: req.body.telefono,
        encabezado: req.body.encabezado,
        nombre: req.body.nombre,
        clausulas: req.body.clausulas
    });
    await modelo.save();
    res.json({"estado": "Formato Guardado."});
}

/**Edita un documento segun el _id en la coleccion Formato */
controladorFormato.editar = async (req, res) => 
{
    const modelo = new modeloFormato({
        _id: req.params.id,
        arrendador: req.body.arrendador,
        ciudad: req.body.ciudad,
        documento: req.body.documento,
        direccion: req.body.direccion,
        telefono: req.body.telefono,
        encabezado: req.body.encabezado,
        nombre: req.body.nombre,
        clausulas: req.body.clausulas
    });
  
    await modeloFormato.findByIdAndUpdate(req.params.id, {$set: modelo}, {new: true});
    res.json({"estado": "Formato Actualizado."});
}

/**Elimina un documento segun el _id en la coleccion Formato */
controladorFormato.eliminar = async (req, res) => 
{
    await modeloFormato.findByIdAndDelete(req.params.id);
    res.json({"estado": "Formato Eliminado."});
}

module.exports = controladorFormato;