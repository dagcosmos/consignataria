const controladorVehiculo = {};
const modeloVehiculo = require('../modelos/vehiculo');
const modeloContrato = require('../modelos/contrato');

/**Devuelve todos los documentos de la coleccion Vehiculo*/
controladorVehiculo.traerTodos = async (req, res) => 
{
    const registros = await modeloVehiculo.find();
    res.json(registros);
}

/**Devuelve un solo documento con el _id de la coleccion Vehiculo*/
controladorVehiculo.traer = async (req, res) => 
{
    const registro = await modeloVehiculo.findById(req.params.id);
    res.json(registro);
}

/**Devuelve los contratos donde se han asociado un Vehiculo*/
controladorVehiculo.traerContratos = async (req, res) => 
{
    const registro = await modeloContrato.find( {_id_vehiculo: req.params.id} )
    .populate({ path: '_id_vehiculo' })
    .populate({ path: '_id_arrendatario' })
    .exec();
    res.json(registro);
}

/**Crea un documento en la coleccion Vehiculo*/
controladorVehiculo.crear = async (req, res) => 
{
    const modelo = new modeloVehiculo({        
        vehiculo: req.body.vehiculo,
        marca: req.body.marca,
        modelo: req.body.modelo,
        tipo: req.body.tipo,
        color: req.body.color,
        manifiesto: req.body.manifiesto,
        placa: req.body.placa,
        chasis: req.body.chasis,
        servicio: req.body.servicio,
        precio: req.body.precio,
        seguro: req.body.seguro,
        vence: req.body.vence,
        numero_seguro: req.body.numero_seguro,
        tecnomecanica: req.body.tecnomecanica,
        propietario: req.body.propietario,
        documento: req.body.documento
    });
    await modelo.save();
    res.json({"estado": "Vehiculo Guardado."});
}

/**Edita un documento segun el _id en la coleccion Vehiculo */
controladorVehiculo.editar = async (req, res) => 
{
    const modelo = new modeloVehiculo({
        _id: req.params.id,
        vehiculo: req.body.vehiculo,
        marca: req.body.marca,
        modelo: req.body.modelo,
        tipo: req.body.tipo,
        color: req.body.color,
        manifiesto: req.body.manifiesto,
        placa: req.body.placa,
        chasis: req.body.chasis,
        servicio: req.body.servicio,
        precio: req.body.precio,
        seguro: req.body.seguro,
        vence: req.body.vence,
        numero_seguro: req.body.numero_seguro,
        tecnomecanica: req.body.tecnomecanica,
        propietario: req.body.propietario,
        documento: req.body.documento
    });
  
    await modeloVehiculo.findByIdAndUpdate(req.params.id, {$set: modelo}, {new: true});
    res.json({"estado": "Vehiculo Actualizado."});
}

/**Elimina un documento segun el _id en la coleccion Vehiculo */
controladorVehiculo.eliminar = async (req, res) => 
{
    await modeloVehiculo.findByIdAndDelete(req.params.id);
    res.json({"estado": "Vehiculo Eliminado."});
}

module.exports = controladorVehiculo;