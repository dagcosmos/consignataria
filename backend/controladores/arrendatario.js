const controladorArrendatario = {};
const modeloArrendatario = require('../modelos/arrendatario');
const modeloContrato = require('../modelos/contrato');

/**Devuelve todos los documentos de la coleccion Arrendatario*/
controladorArrendatario.traerTodos = async (req, res) => 
{
    const registros = await modeloArrendatario.find();
    res.json(registros);
}

/**Devuelve un solo documento con el _id de la coleccion Arrendatario*/
controladorArrendatario.traer = async (req, res) => 
{
    const registro = await modeloArrendatario.findById(req.params.id);
    res.json(registro);
}

/**Devuelve los contratos que han sido asociados a un Arrendatario*/
controladorArrendatario.traerContratos = async (req, res) => 
{
    const registro = await modeloContrato.find( {_id_arrendatario: req.params.id} )
    .populate({ path: '_id_arrendatario' })
    .populate({ path: '_id_vehiculo' })
    .exec();
    res.json(registro);
}

/**Crea un documento en la coleccion Arrendatario*/
controladorArrendatario.crear = async (req, res) => 
{
    const modelo = new modeloArrendatario({
        nombres: req.body.nombres,
        apellidos: req.body.apellidos,
        documento: req.body.documento,
        direccion: req.body.direccion,
        telefono: req.body.telefono
    });
    await modelo.save();
    res.json({"estado": "Arrendatario Guardado."});
}

/**Edita un documento segun el _id en la coleccion Arrendatario */
controladorArrendatario.editar = async (req, res) => 
{
    const modelo = new modeloArrendatario({
        _id: req.params.id,
        nombres: req.body.nombres,
        apellidos: req.body.apellidos,
        documento: req.body.documento,
        direccion: req.body.direccion,
        telefono: req.body.telefono 
    });
  
    await modeloArrendatario.findByIdAndUpdate(req.params.id, {$set: modelo}, {new: true});    
    res.json({"estado": "Arrendatario Actualizado."});
}

/**Elimina un documento segun el _id en la coleccion Arrendatario */
controladorArrendatario.eliminar = async (req, res) => 
{
    await modeloArrendatario.findByIdAndDelete(req.params.id);
    res.json({"estado": "Arrendatario Eliminado."});
}

module.exports = controladorArrendatario;