import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConGenerarComponent } from './con-generar.component';

describe('ConGenerarComponent', () => {
  let component: ConGenerarComponent;
  let fixture: ComponentFixture<ConGenerarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConGenerarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConGenerarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
