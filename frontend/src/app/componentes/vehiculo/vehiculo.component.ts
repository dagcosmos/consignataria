import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { VehiculoService } from '../../servicios/vehiculo.service';
import { Vehiculo } from 'src/app/modelos/vehiculo';

@Component({
  selector: 'app-vehiculo',
  templateUrl: './vehiculo.component.html',
  styleUrls: ['./vehiculo.component.css'],
  providers: [VehiculoService]
})
export class VehiculoComponent implements OnInit {

  ocultarformularioNuevo: boolean = true;
  msg: String = "";
  mostrarMenu: boolean = false;
  accion: string;
  dataTable: any;

  constructor(private vehiculoService: VehiculoService) { }

  ngOnInit() 
  {
    this.obtener();
  }

  obtener(): void
  {
    this.vehiculoService.obtenerTodos()
    .subscribe( respuesta => {
      this.vehiculoService.vehiculos = respuesta as Vehiculo[];
    });
  }

  nuevo(formulario: NgForm): void
  {
    if( formulario.value._id ){

      this.vehiculoService.editar(formulario.value)
      .subscribe(res => {
        this.obtener();
        this.msg = "Vehiculo Actualizado.";
        this.cerrarformNuevo(formulario);
      });

    }else{

      this.vehiculoService.crear(formulario.value)    
      .subscribe( respuesta => {
        this.obtener();
        this.msg = "Vehiculo Guardado.";
        this.cerrarformNuevo(formulario);
      });
    }    
  }

  editar(vehiculo: Vehiculo): void
  {
    this.vehiculoService.vehiculoSeleccionado = vehiculo;
    this.ocultarformularioNuevo = false;
    this.accion = "Editar";   
  }

  eliminar(_id: string): void
  {
    if( confirm("¿Borrar el Registro?") ){
      this.vehiculoService.eliminar(_id)
      .subscribe( respuesta => {
        this.obtener();
        this.msg = "Vehiculo Eliminado.";
      });
    }    
  }

  abrirformNuevo(formulario?: NgForm): void
  {
    if(this.ocultarformularioNuevo){
      this.ocultarformularioNuevo = false;
      this.accion = "Nuevo";
    }      
    else{
      this.cerrarformNuevo(formulario); 
      this.ocultarformularioNuevo = false;
      this.accion = "Nuevo"; 
    }       
  }  

  cerrarformNuevo(formulario?: NgForm): void
  {
    if(formulario){
      formulario.reset();
      this.obtener();
      this.vehiculoService.vehiculoSeleccionado = new Vehiculo();
      this.ocultarformularioNuevo = true;
      this.accion = "";
    }
  }   
    
  closeAlert():void
  {
    this.msg = "";
  }

}
