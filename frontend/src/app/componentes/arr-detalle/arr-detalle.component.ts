import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ArrendatarioService } from '../../servicios/arrendatario.service';

@Component({
  selector: 'app-arr-detalle',
  templateUrl: './arr-detalle.component.html',
  styleUrls: ['./arr-detalle.component.css'],
  providers: [ArrendatarioService]
})
export class ArrDetalleComponent implements OnInit {

  id: string;
  arrendatario: string;
  contratos: any;
  

  constructor(private ruta: ActivatedRoute, 
    private arrendatarioService: ArrendatarioService) {
    this.ruta.params.subscribe( parametros => {
      this.id = parametros['id'];
    });
   }

  ngOnInit() 
  {
    this.arrendatarioService.obtenerContratos(this.id)
    .subscribe(respuesta => {
      this.arrendatario = respuesta[0]._id_arrendatario.nombres + " " + respuesta[0]._id_arrendatario.apellidos;
      this.contratos = respuesta;      
    });
  }

}
