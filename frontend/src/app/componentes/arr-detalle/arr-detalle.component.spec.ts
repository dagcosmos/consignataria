import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArrDetalleComponent } from './arr-detalle.component';

describe('ArrDetalleComponent', () => {
  let component: ArrDetalleComponent;
  let fixture: ComponentFixture<ArrDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArrDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArrDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
