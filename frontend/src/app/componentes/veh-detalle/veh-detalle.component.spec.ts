import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehDetalleComponent } from './veh-detalle.component';

describe('VehDetalleComponent', () => {
  let component: VehDetalleComponent;
  let fixture: ComponentFixture<VehDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
