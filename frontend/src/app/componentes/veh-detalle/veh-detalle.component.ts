import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { VehiculoService } from '../../servicios/vehiculo.service';

@Component({
  selector: 'app-veh-detalle',
  templateUrl: './veh-detalle.component.html',
  styleUrls: ['./veh-detalle.component.css'],
  providers: [VehiculoService]
})
export class VehDetalleComponent implements OnInit {

  id: string;
  vehiculo: string;
  contratos: any;

  constructor(private ruta: ActivatedRoute, 
    private vehiculoService: VehiculoService) {
    this.ruta.params.subscribe( parametros => {
      this.id = parametros['id'];
    });
   }

  ngOnInit() 
  {
    this.vehiculoService.obtenerContratos(this.id)
    .subscribe(respuesta => {
      this.vehiculo = respuesta[0]._id_vehiculo.marca  + " " + respuesta[0]._id_vehiculo.color + " " + respuesta[0]._id_vehiculo.placa;
      this.contratos = respuesta; 
          
    });
  }

}
