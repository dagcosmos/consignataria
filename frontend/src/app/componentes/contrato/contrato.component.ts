import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ContratoService } from '../../servicios/contrato.service';
import { Contrato } from 'src/app/modelos/contrato';

@Component({
  selector: 'app-contrato',
  templateUrl: './contrato.component.html',
  styleUrls: ['./contrato.component.css'],
  providers: [ContratoService]
})
export class ContratoComponent implements OnInit {

  ocultarformularioNuevo: boolean = true;
  msg: String = "";
  mostrarMenu: boolean = false;
  accion: string;
  dataTable: any;

  constructor(private contratoService: ContratoService){}

  ngOnInit() 
  {
    this.obtener();
  }

  obtener(): void
  {
    this.contratoService.obtenerTodos()
    .subscribe( respuesta => {
      this.contratoService.contratos = respuesta as Contrato[];
    });
  }

  nuevo(formulario: NgForm): void
  {
    if( formulario.value._id ){

      this.contratoService.editar(formulario.value)
      .subscribe(res => {
        this.obtener();
        this.msg = "Contrato Actualizado.";
        this.cerrarformNuevo(formulario);
      });

    }else{

      this.contratoService.crear(formulario.value)    
      .subscribe( respuesta => {
        this.obtener();
        this.msg = "Contrato Guardado.";
        this.cerrarformNuevo(formulario);
      });
    }    
  }

  editarArrendatario(contrato: Contrato): void
  {
    this.contratoService.contratoSeleccionado = contrato;
    this.ocultarformularioNuevo = false;
    this.accion = "Editar";   
  }

  eliminar(_id: string): void
  {
    if( confirm("¿Borrar el Registro?") ){
      this.contratoService.eliminar(_id)
      .subscribe( respuesta => {
        this.obtener();
        this.msg = "Contrato Eliminado.";
      });
    }    
  }

  abrirformNuevo(formulario?: NgForm): void
  {
    if(this.ocultarformularioNuevo){
      this.ocultarformularioNuevo = false;
      this.accion = "Nuevo";
    }      
    else{
      this.cerrarformNuevo(formulario); 
      this.ocultarformularioNuevo = false;
      this.accion = "Nuevo"; 
    }       
  }  

  cerrarformNuevo(formulario?: NgForm): void
  {
    if(formulario){
      formulario.reset();
      this.obtener();
      this.contratoService.contratoSeleccionado = new Contrato();
      this.ocultarformularioNuevo = true;
      this.accion = "";
    }
  }   
    
  closeAlert():void
  {
    this.msg = "";
  }

}
