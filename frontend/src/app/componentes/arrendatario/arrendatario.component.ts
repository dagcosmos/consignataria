import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ArrendatarioService } from '../../servicios/arrendatario.service';
import { Arrendatario } from 'src/app/modelos/arrendatario';

@Component({
  selector: 'app-arrendatario',
  templateUrl: './arrendatario.component.html',
  styleUrls: ['./arrendatario.component.css'],
  providers: [ArrendatarioService]
})
export class ArrendatarioComponent implements OnInit {

  ocultarformularioNuevo: boolean = true;
  msg: String = "";
  mostrarMenu: boolean = false;
  accion: string;
  dataTable: any;

  constructor(private arrendatarioService: ArrendatarioService){}

  ngOnInit() 
  {
    this.obtener();
  }

  obtener(): void
  {
    this.arrendatarioService.obtenerTodos()
    .subscribe( respuesta => {
      this.arrendatarioService.arrendatarios = respuesta as Arrendatario[];
    });
  }

  nuevo(formulario: NgForm): void
  {
    if( formulario.value._id ){

      this.arrendatarioService.editar(formulario.value)
      .subscribe(res => {
        this.obtener();
        this.msg = "Arrendatario Actualizado.";
        this.cerrarformNuevo(formulario);
      });

    }else{

      this.arrendatarioService.crear(formulario.value)    
      .subscribe( respuesta => {
        this.obtener();
        this.msg = "Arrendatario Guardado.";
        this.cerrarformNuevo(formulario);
      });
    }    
  }

  editar(arrendatario: Arrendatario): void
  {
    this.arrendatarioService.arrendatarioSeleccionado = arrendatario;
    this.ocultarformularioNuevo = false;
    this.accion = "Editar";   
  }

  eliminar(_id: string): void
  {
    if( confirm("¿Borrar el Registro?") ){
      this.arrendatarioService.eliminar(_id)
      .subscribe( respuesta => {
        this.obtener();
        this.msg = "Arrendatario Eliminado.";
      });
    }    
  }

  abrirformNuevo(formulario?: NgForm): void
  {
    if(this.ocultarformularioNuevo){
      this.ocultarformularioNuevo = false;
      this.accion = "Nuevo";
    }      
    else{
      this.cerrarformNuevo(formulario); 
      this.ocultarformularioNuevo = false;
      this.accion = "Nuevo"; 
    }       
  }  

  cerrarformNuevo(formulario?: NgForm): void
  {
    if(formulario){
      formulario.reset();
      this.obtener();
      this.arrendatarioService.arrendatarioSeleccionado = new Arrendatario();
      this.ocultarformularioNuevo = true;
      this.accion = "";
    }
  }   
    
  closeAlert():void
  {
    this.msg = "";
  }
}