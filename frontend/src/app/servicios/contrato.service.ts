import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Contrato } from '../modelos/contrato';

@Injectable({
  providedIn: 'root'
})
export class ContratoService {

  readonly url_api = 'http://127.0.0.1:3000/contrato';
  contratoSeleccionado: Contrato;
  contratos: Contrato[];

constructor(private http: HttpClient) 
{
  this.contratoSeleccionado = new Contrato();
}

  obtenerTodos()
  {
    return this.http.get(this.url_api);
  }

  obtenerUno(id:string)
  {
    return this.http.get(`${this.url_api}/${id}`);
  }

  detalle(id:string)
  {
    return this.http.get(`${this.url_api}/detalle/${id}`);
  }

  crear(contrato: Contrato)
  {
    return this.http.post(this.url_api, contrato);
  }

  editar(contrato: Contrato)
  {
    return this.http.put(`${this.url_api}/${contrato._id}`, contrato); 
  }

  eliminar(id: string)
  {
    return this.http.delete(`${this.url_api}/${id}`);
  }
}