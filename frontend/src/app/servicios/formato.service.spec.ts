import { TestBed, inject } from '@angular/core/testing';

import { FormatoService } from './formato.service';

describe('FormatoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FormatoService]
    });
  });

  it('should be created', inject([FormatoService], (service: FormatoService) => {
    expect(service).toBeTruthy();
  }));
});
