import { TestBed, inject } from '@angular/core/testing';

import { ArrendatarioService } from './arrendatario.service';

describe('ArrendatarioService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ArrendatarioService]
    });
  });

  it('should be created', inject([ArrendatarioService], (service: ArrendatarioService) => {
    expect(service).toBeTruthy();
  }));
});
