import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Arrendatario } from '../modelos/arrendatario';

@Injectable({
  providedIn: 'root'
})
export class ArrendatarioService {

  readonly url_api = 'http://127.0.0.1:3000/arrendatario';
  arrendatarioSeleccionado: Arrendatario;
  arrendatarios: Arrendatario[];

constructor(private http: HttpClient) 
{
  this.arrendatarioSeleccionado = new Arrendatario();
}

  obtenerTodos()
  {
    return this.http.get(this.url_api);
  }

  obtenerUno(id:string)
  {
    return this.http.get(`${this.url_api}/${id}`);
  }

  obtenerContratos(id:string)
  {
    return this.http.get(`${this.url_api}/contratos/${id}`);
  }

  crear(arrendatario: Arrendatario)
  {
    return this.http.post(this.url_api, arrendatario);
  }

  editar(arrendatario: Arrendatario)
  {
    return this.http.put(`${this.url_api}/${arrendatario._id}`, arrendatario); 
  }

  eliminar(id: string)
  {
    return this.http.delete(`${this.url_api}/${id}`);
  }

}