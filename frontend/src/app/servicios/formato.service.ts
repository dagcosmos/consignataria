import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Formato } from '../modelos/formato';


@Injectable({
  providedIn: 'root'
})
export class FormatoService {

  readonly url_api = 'http://127.0.0.1:3000/formato';
  formatoSeleccionado: Formato;
  formatos: Formato[];

constructor(private http: HttpClient) 
{
  this.formatoSeleccionado = new Formato();
}

  obtenerTodos()
  {
    return this.http.get(this.url_api);
  }

  obtenerUno(id:string)
  {
    return this.http.get(`${this.url_api}/${id}`);
  }

   crear(formato: Formato)
  {
    return this.http.post(this.url_api, formato);
  }

  editar(formato: Formato)
  {
    return this.http.put(`${this.url_api}/${formato._id}`, formato); 
  }

  eliminar(id: string)
  {
    return this.http.delete(`${this.url_api}/${id}`);
  }
}
