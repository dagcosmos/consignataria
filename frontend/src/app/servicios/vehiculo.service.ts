import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Vehiculo } from '../modelos/vehiculo';

@Injectable({
  providedIn: 'root'
})
export class VehiculoService {

  readonly url_api = 'http://127.0.0.1:3000/vehiculo';
  vehiculoSeleccionado: Vehiculo;
  vehiculos: Vehiculo[];

constructor(private http: HttpClient) 
{
  this.vehiculoSeleccionado = new Vehiculo();
}

  obtenerTodos()
  {
    return this.http.get(this.url_api);
  }

  obtenerUno(id:string)
  {
    return this.http.get(`${this.url_api}/${id}`);
  }

  obtenerContratos(id:string)
  {
    return this.http.get(`${this.url_api}/contratos/${id}`);
  }

  crear(vehiculo: Vehiculo)
  {
    return this.http.post(this.url_api, vehiculo);
  }

  editar(vehiculo: Vehiculo)
  {
    return this.http.put(`${this.url_api}/${vehiculo._id}`, vehiculo); 
  }

  eliminar(id: string)
  {
    return this.http.delete(`${this.url_api}/${id}`);
  }
}
