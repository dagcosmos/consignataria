export class Formato {

    _id: string;
    arrendador: string;
    ciudad: string;
    documento: number;
    direccion: string;
    telefono: number;
    encabezado: string;
    nombre:string;
    clausulas: string;

    constructor(_id='', arrendador='', ciudad='', documento=0, direccion='', telefono=0, 
        encabezado='', nombre='', clausulas='')
    {
        this._id = _id;
        this.arrendador = arrendador;
        this.ciudad = ciudad;
        this.documento = documento;
        this.direccion = direccion;
        this.telefono = telefono;
        this.encabezado = encabezado;
        this.nombre = nombre;
        this.clausulas = clausulas;
    }  
}
