export class Arrendatario {

    _id: string;
    nombres: string;
    apellidos: string;
    documento: string;
    direccion: string;
    telefono: string

    constructor(_id='', nombres='', apellidos='', documento='', direccion='', telefono='')
    {
        this._id = _id;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.documento = documento;
        this.direccion = direccion;
        this.telefono = telefono;
    }    
}
