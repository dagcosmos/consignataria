export class Contrato {

    _id: string;
    fecha: number;
    hora: string;
    valor: number;
    vigencia: number;
    antelacion_prorroga: number;
    _id_vehiculo: string;
    _id_arrendatario:string;

    constructor(_id='', valor=0, antelacion_prorroga=0, _id_vehiculo="", _id_arrendatario="")
    {
        let ahora = new Date();
        this._id = _id;
        this.fecha = Date.now();
        this.hora = ahora.getHours() + ':' + ahora.getMinutes() + ':' + ahora.getSeconds();
        this.valor = valor;
        this.vigencia = Date.now();
        this.antelacion_prorroga = antelacion_prorroga;
        this._id_vehiculo = _id_vehiculo;
        this._id_arrendatario = _id_arrendatario;
    }    
}
