export class Vehiculo {

    _id: string;
    vehiculo: string;
    marca: string;
    modelo: number;
    tipo: string;
    color: string;
    manifiesto: string;
    placa: string;
    chasis: string;
    servicio: string;
    precio: number;
    seguro: string;
    vence: string;
    numero_seguro: string;
    tecnomecanica: string;
    propietario: string;
    documento: number;

    constructor(_id='', vehiculo='MOTOCLICLETA', marca='NULL', modelo=0, tipo='TURISMO', color='NULL', manifiesto='', 
        placa='', chasis='', servicio='PARTICULAR', precio=0, seguro='SI', vence='SI', numero_seguro='SI', 
        tecnomecanica='SI', propietario='', documento=0)
    {
        this._id = _id;
        this.vehiculo = vehiculo;
        this.marca = marca;
        this.modelo = modelo;
        this.tipo = tipo;
        this.color = color;
        this.manifiesto = manifiesto;
        this.placa = placa;
        this.chasis = chasis;
        this.servicio = servicio;
        this.precio = precio;
        this.seguro = seguro;
        this.vence = vence;
        this.numero_seguro = numero_seguro;
        this.tecnomecanica = tecnomecanica;
        this.propietario = propietario;
        this.documento = documento;
    }    
}
