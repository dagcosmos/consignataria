import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import {DataTableModule} from "angular-6-datatable";

import { MenuComponent } from './componentes/menu/menu.component';
import { AppComponent } from './app.component';
import { EncabezadoComponent } from './componentes/encabezado/encabezado.component';
import { PieComponent } from './componentes/pie/pie.component';
import { InicioComponent } from './componentes/inicio/inicio.component';
import { ArrendatarioComponent } from './componentes/arrendatario/arrendatario.component';
import { VehiculoComponent } from './componentes/vehiculo/vehiculo.component';
import { ContratoComponent } from './componentes/contrato/contrato.component';
import { FormatoComponent } from './componentes/formato/formato.component';
import { ArrDetalleComponent } from './componentes/arr-detalle/arr-detalle.component';
import { VehDetalleComponent } from './componentes/veh-detalle/veh-detalle.component';
import { ConGenerarComponent } from './componentes/con-generar/con-generar.component';

const rutas: Routes = [ 
  { path: 'arrendatario', component: ArrendatarioComponent },
  { path: 'vehiculo', component: VehiculoComponent },
  { path: 'contrato', component: ContratoComponent },
  { path: 'con-generar/:id', component: ConGenerarComponent },
  { path: 'formato', component: FormatoComponent },  
  { path: 'arr-detalle/:id', component: ArrDetalleComponent },
  { path: 'veh-detalle/:id', component: VehDetalleComponent } , 
  { path: '**', redirectTo: 'inicio', pathMatch: 'full' },
  { path: '', component: InicioComponent, pathMatch: 'full', data: {mostrarMenu: false} },
];

@NgModule({
  declarations: [
    MenuComponent,
    AppComponent,    
    EncabezadoComponent,
    PieComponent,   
    InicioComponent,
    ArrendatarioComponent,
    VehiculoComponent,
    ContratoComponent,
    FormatoComponent,
    ArrDetalleComponent,
    VehDetalleComponent,
    ConGenerarComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(rutas),
    DataTableModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
